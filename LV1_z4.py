def count_words(filename='song (1).txt'):
    word_count = {}
    with open('song (1).txt', 'r') as file:
        for line in file:
            words = line.split()
            for word in words:
                word = word.lower()
                word_count[word] = word_count.get(word, 0) + 1
    return word_count

def main():
    word_count = count_words("song.txt")

    single_occurrence_words = [word for word, count in word_count.items() if count == 1]

    print("Broj riječi koje se pojavljuju samo jednom:", len(single_occurrence_words))
    print("Riječi koje se pojavljuju samo jednom:", single_occurrence_words)

main()