def read_sms_file(filename="SMSSpamCollection (1).txt"):
    hams = []
    spams = []
    with open("SMSSpamCollection (1).txt", 'r') as file:
        for line in file:
            parts = line.strip().split('\t')
            if len(parts) == 2:
                label, message = parts
                if label.lower() == "ham":
                    hams.append(message)
                elif label.lower() == "spam":
                    spams.append(message)
    return hams, spams

def average_words(messages):
    total_words = sum(len(message.split()) for message in messages)
    return total_words / len(messages) if messages else 0

def count_exclamation(messages):
    return sum(1 for message in messages if message.endswith('!'))

def main():
    hams, spams = read_sms_file("OSU-LV1\SMSSpamCollection (1).txt")

    avg_words_ham = average_words(hams)
    avg_words_spam = average_words(spams)

    print("Prosječan broj riječi u porukama označenim kao 'ham':", avg_words_ham)
    print("Prosječan broj riječi u porukama označenim kao 'spam':", avg_words_spam)

    exclamation_count = count_exclamation(spams)
    print("Broj SMS poruka koje su tipa spam, a završavaju uskličnikom:", exclamation_count)

main()