import numpy as np
from tensorflow import keras
from keras.models import load_model
from matplotlib import pyplot as plt
from PIL import Image

model = load_model("LV8/FCN/model.keras")

image = Image.open("LV8/broj.png")
image = image.convert("L")
image = image.resize((28, 28))

image_array = np.array(image)
image_array = image_array.astype("float32") / 255
image_array = np.expand_dims(image_array, axis=0)



predictions = model.predict(image_array)
print(predictions)
predicted_class = np.argmax(predictions, axis=1)


plt.imshow(image)
plt.title(f'Predikcija: {predicted_class[0]}')
plt.show()


