import numpy as np
from tensorflow import keras
from keras.models import load_model
from matplotlib import pyplot as plt

(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

model = load_model("LV8/FCN/model.keras")

y_pred_s = model.predict(x_test_s)
y_pred = np.argmax(y_pred_s, axis=1)
incorrect_indices = np.where(y_pred != y_test)[0]

num_images_to_show = 5
for i in range(min(num_images_to_show, len(incorrect_indices))):
    index = incorrect_indices[i]
    misclassified_image = x_test_s[index].reshape((28,28))
    true_label = y_test[index]
    predicted_label = y_pred[index]

    plt.imshow(misclassified_image, cmap="gray")
    plt.title(f"True value: {true_label}, Predicted value: {predicted_label}")
    plt.axis("off")
    plt.show()

