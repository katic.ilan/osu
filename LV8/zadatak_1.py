import numpy as np
import seaborn as sns
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix

# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa
fig, axes = plt.subplots(1, 6, figsize=(15, 3))
for i in range(6):
    axes[i].imshow(x_train[i], cmap='gray')
    axes[i].axis('off')

plt.show()

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")

# train skup ima 60000 podataka, a train skup ima 10000
# ulazna velicina je kodirana kao 28x28 piksela slika gdje se oznacava brojem 1 dio slike koji je obojan a 0 onaj koji nije, izlazna velcina je kodirana kao vektor gdje svaki element predstavlja broj od 0 do 10, oznacava koji je broj napisan na ulaznoj slici, kasnije se to pretvori u kategoricku velicinu pa element nije broj nego je element vektora vektor dimenzija (10, 1) i znamenku 1 u onom retku koji je broj napisan na ulaznoj slici, ako je broj 5 napisan znamenka 1 ce biti u 5. retku a ostalo ce biti 0, ako je 0 napisana, u nultom retku ce biti jedinica a ostalo nule



# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

print(0)
# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
model = keras.Sequential()
model.add(layers.Flatten(input_shape=(28, 28, 1)))
model.add(layers.Dense(100, activation="relu"))
model.add(layers.Dense(50, activation="relu"))
model.add(layers.Dense(10, activation="softmax"))

model.summary()

# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
model.compile(
    loss="categorical_crossentropy",
    optimizer="adam",
    metrics=["accuracy",]
)

# TODO: provedi ucenje mreze
batch_size = 32
epochs = 20
history = model.fit(
    x_train_s, 
    y_train_s,
    batch_size = batch_size,
    epochs = epochs,
    validation_split = 0.1
    )

# TODO: Prikazi test accuracy i matricu zabune
test_loss, test_acc = model.evaluate(x_test_s, y_test_s, verbose=0)
print(test_acc)
predictions = model.predict(x_test_s)
y_predict = np.argmax(predictions, axis=1)
conf_matrix = confusion_matrix(np.argmax(y_test_s, axis=1), y_predict)
plt.figure(figsize=(10, 8))
sns.heatmap(conf_matrix, annot=True, fmt='d', cmap='YlGnBu')
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# TODO: spremi model

model.save("LV8/FCN/model.keras")
del model


