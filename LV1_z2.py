try:
    grade = float(input("Upisi ocjenu: "))
    if 0.0 <= grade <=1.0:
        if(0.9<=grade<=1.0):
            print("A")
        if(0.8<=grade<0.9):
            print("B")
        if(0.7<=grade<0.8):
            print("C")
        if(0.6<=grade<0.7):
            print("D")
        if(0.5<=grade<0.6):
            print("E")
        if(0<=grade<0.5):
            print("F")
    else:
        print("Unešena kriva ocjena")
except ValueError:
        print("Greška: Niste unijeli broj.")