numbers = []

while True:
    num = input("Unesi broj: ")
    if num == "Done":
        break
    else: 
        number=float(num)
        numbers.append(number)
    

print("Unešeno je: ", len(numbers), "brojeva")
print("Max:", max(numbers),"\nMin: ",min(numbers), "\nProsjek: ", sum(numbers)/len(numbers))

numbers.sort()
print("Sortirana lista: ", numbers)