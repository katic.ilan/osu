import numpy as np
import matplotlib.pyplot as plt

crni_kvadrat = np.zeros((50, 50))
bijeli_kvadrat = np.ones((50, 50))

gornji_red = np.hstack((crni_kvadrat, bijeli_kvadrat))

donji_red = np.hstack((bijeli_kvadrat, crni_kvadrat))

slika = np.vstack((gornji_red, donji_red))

plt.imshow(slika, cmap='binary') 
plt.show()
