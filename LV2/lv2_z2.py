import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt('data.csv', delimiter=',', skiprows=1)

broj_osoba = data.shape[0]
print("Broj osoba za koje su izvršena mjerenja:", broj_osoba)


visina = data[::50, 1]
masa = data[::50, 2]
plt.figure(figsize=(8, 6))  
plt.scatter(visina, masa, color='pink', alpha=0.8)  
plt.xlabel('Visina (cm)')
plt.ylabel('Masa (kg)')
plt.title('Odnos visine i mase osoba')
plt.grid(True)
plt.show()


minimalna_visina = np.min(visina)
maksimalna_visina = np.max(visina)
srednja_visina = np.mean(visina)
print("Minimalna visina:", minimalna_visina, "cm")
print("Maksimalna visina:", maksimalna_visina, "cm")
print("Srednja visina:", srednja_visina, "cm")


muskarci = data[data[:, 0] == 1]
zene = data[data[:, 0] == 0]
visina_muskarci = muskarci[:, 1]
masa_muskarci = muskarci[:, 2]
visina_zene = zene[:, 1]
masa_zene = zene[:, 2]
plt.figure(figsize=(8, 6))
plt.scatter(visina_muskarci, masa_muskarci, color='blue', alpha=0.5, label='Muškarci') 
plt.scatter(visina_zene, masa_zene, color='red', alpha=0.5, label='Žene')  
plt.xlabel('Visina (cm)')
plt.ylabel('Masa (kg)')
plt.title('Odnos visine i mase muškaraca i žena')
plt.legend()
plt.grid(True)
plt.show()