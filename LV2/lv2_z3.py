import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

slika = mpimg.imread('road.jpg')
visina, sirina, _ = slika.shape
faktor_posvjetljivanja = 50
slika_posvijetljena = np.clip(slika.astype(int) + faktor_posvjetljivanja, 0, 255)
plt.figure(figsize=(10, 5))
plt.subplot(1, 2, 1)
plt.imshow(slika)
plt.title('Originalna slika')
plt.subplot(1, 2, 2)
plt.imshow(slika_posvijetljena)
plt.title('Posvijetljena slika')
plt.show()


pocetak_sirina = sirina // 2
kraj_sirina = sirina
druga_cetvrtina_slike = slika[:, pocetak_sirina:kraj_sirina, :]
plt.imshow(druga_cetvrtina_slike)
plt.title('Druga četvrtina slike po širini')
plt.axis('off')
plt.show()


rotirana_slika = np.rot90(slika, k=3)
plt.figure(figsize=(10, 5))
plt.subplot(1, 2, 1)
plt.imshow(slika)
plt.title('Originalna slika')
plt.subplot(1, 2, 2)
plt.imshow(rotirana_slika)
plt.title('Rotirana slika')
plt.show()



zrcaljena_slika = np.fliplr(slika)
plt.figure(figsize=(10, 5))
plt.subplot(1, 2, 1)
plt.imshow(slika)
plt.title('Originalna slika')
plt.subplot(1, 2, 2)
plt.imshow(zrcaljena_slika)
plt.title('Zrcaljena slika')
plt.show()

